import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Genre } from '../../model/genre';

@Injectable()
export class GenreService {

  private _path = '/api/genre';

  constructor(private apiService: ApiService) { }

  getAll(): Observable<Genre[]> {
    return this.apiService.get(`${this._path}`);
  }

  save(item: Genre): Observable<Genre> {
    return this.apiService.post(`${this._path}`, item);
  }

}
