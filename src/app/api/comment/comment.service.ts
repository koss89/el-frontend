import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Comment } from '../../model/comment';

@Injectable()
export class CommentService {

  constructor(private apiService: ApiService) { }

  getBookComments(page: number, idBook: number): Observable<any> {
    return this.apiService.get(`/api/book/${idBook}/comments?page=${page}`);
  }

  save(body: Comment, idBook: number): Observable<Comment> {
    return this.apiService.post(`/api/book/${idBook}/comments`, body);
  }

  saveSub(body: Comment, idBook: number, idparent: number): Observable<Comment> {
    return this.apiService.post(`/api/book/${idBook}/comments?parent=${idparent}`, body);
  }

}
