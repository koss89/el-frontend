import { NgModule, ModuleWithProviders  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient, HttpXhrBackend } from '@angular/common/http';
import { ApiServiceConfig } from './api-config';
import { ApiService } from './api/api.service';
import { AuthorService } from './author/author.service';
import { GenreService } from './genre/genre.service';
import { BookService } from './book/book.service';
import { TagService } from './tag/tag.service';
import { CommentService } from './comment/comment.service';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule
  ],
  declarations: [],
  providers: [ApiService, AuthorService, GenreService, BookService, TagService, CommentService]
})
export class ApiModule {
  static forRoot(config: ApiServiceConfig): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers: [
        {provide: ApiServiceConfig, useValue: config }
      ]
    };
  }
 }
