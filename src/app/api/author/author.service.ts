import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Author } from '../../model/author';

@Injectable()
export class AuthorService {

  private _path = '/api/author';

  constructor(private apiService: ApiService) { }

  getAll(): Observable<Author[]> {
    return this.apiService.get(`${this._path}`);
  }

  save(author: Author): Observable<Author> {
    return this.apiService.post(`${this._path}`, author);
  }

}
