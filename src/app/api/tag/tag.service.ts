import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Tag } from '../../model/tag';

@Injectable()
export class TagService {

  private _path = '/api/tag';

  constructor(private apiService: ApiService) { }

  getAll(): Observable<Tag[]> {
    return this.apiService.get(`${this._path}`);
  }

  save(item: Tag): Observable<Tag> {
    return this.apiService.post(`${this._path}`, item);
  }

}
