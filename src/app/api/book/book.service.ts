import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Book } from '../../model/book';

@Injectable()
export class BookService {

  private _path = '/api/book';

  constructor(private apiService: ApiService) { }

  getBooks(page: number, sort: any): Observable<any> {
    return this.apiService.get(`${this._path}/list?page=${page}&dir=${sort.type}&prop=${sort.prop}`);
  }

  filter(page: number, sort: any, filter: any): Observable<any> {
    return this.apiService.post(`${this._path}/list?page=${page}&dir=${sort.type}&prop=${sort.prop}`, filter);
  }


  getById(id): Observable<Book> {
    return this.apiService.get(`${this._path}/${id}`);
  }

  save(book: Book): Observable<Book> {
    return this.apiService.post(`${this._path}`, book);
  }

  uploadfile(idBook: number, file: any): Observable<any> {
    return this.apiService.post(`/api/storage?idbook=${idBook}`, file);
  }

  getFile(path: string): Observable<any> {
    return this.apiService.getBlob(`${path}`);
  }

  setRating(id: number, rate: number): Observable<any> {
    return this.apiService.post(`${this._path}/${id}/rating`, { rate });
  }

  getRating(id: number): Observable<any> {
    return this.apiService.get(`${this._path}/${id}/rating`);
  }
}
