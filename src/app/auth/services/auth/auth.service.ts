import { Injectable, Optional, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { ApiServiceConfig } from '../../../api/api-config';
import { ToastrService } from 'ngx-toastr';


@Injectable()
export class AuthService {

  private _authUrl: string;
  private token: string;
  public doLogin: EventEmitter<any> = new EventEmitter();

  private roles: Array<any>;

  private _headersConfig = {
    'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Authorization': 'Basic ' + btoa('browser:api'),
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  };


  constructor(@Optional() config: ApiServiceConfig, private router: Router, private http: HttpClient, private toast: ToastrService) {
    this.token = localStorage.getItem('id_token');
    this.roles = JSON.parse(localStorage.getItem('roles'));
    if (config) {
      this._authUrl = config.apiUrl;
    }
  }

  login(username: string, password: string): Observable<boolean> {

    const headers = new HttpHeaders(this._headersConfig);
    const options = { headers: headers };

    return this.http.post<any>(this._authUrl + '/oauth/token?grant_type=password&password='
      + password + '&scope=ui&username=' + username + '', null, options).map(resp => {
        const token = resp && resp.access_token;
        if (token) {
          this.token = token;
          localStorage.setItem('id_token', token);
          this.doLogin.emit();
          this.loadRoles();
          return true;
        } else {
          return false;
        }
      });
  }

  loadRoles() {
    return this.http
      .get<any>(`${this._authUrl}/api/user/current/roles`, { headers: { Authorization: 'Bearer ' + this.token } })
      .subscribe(resp => {
        this.roles = resp;
        localStorage.setItem('roles', JSON.stringify(this.roles));
      }, error => {
        this.toast.error('Не вдалося завантажити', 'ERROR');
      });
  }

  registration(user: any): Observable<any> {
    return this.http.post<any>(`${this._authUrl}/api/user/register`, user);
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    this.roles = null;
    localStorage.removeItem('id_token');
    localStorage.removeItem('roles');
    this.router.navigate(['/login']);
  }

  navigateLogout() {
    this.router.navigate(['/logout']);
  }

  isAutorized(): Boolean {
    if (this.token) {
      // logged in so return true
      return true;
    }
  }

  isAdmin(): Boolean {
    let isadmin = false;
    if (this.roles) {
      this.roles.forEach(element => {
        if (element.role === 'ROLE_ADMIN') {
          isadmin = true;
        }
      });
    }
    return isadmin;
  }

  get Token(): string {
    return this.token;
  }

}
