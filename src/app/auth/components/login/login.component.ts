import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    usr = {
        username: '',
        password: ''
    };

    constructor(private router: Router, private _authService: AuthService, private toast: ToastrService) { }

    ngOnInit() {
        this._authService.logout();
    }

    doLogin() {
        this._authService.login(this.usr.username, this.usr.password).subscribe(
            result => {
                if (result === true) {
                    // login successful
                    this.router.navigate(['/']);
                } else {
                }
            },
            error => {
                this.toast.error('Не вдалося увійти ' + error.status, 'ERROR');
            });
    }

}
