import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  usr = {
    username: '',
    password: ''
  };

  constructor( private router: Router, private _authService: AuthService, private toast: ToastrService) { }

  ngOnInit() {
  }

  doRegister() {
    this._authService.registration(this.usr).subscribe(() => {
      console.log('reg done');
      this.router.navigate(['/login']);
    },
    error => {
        this.toast.error('Не вдалося Зареєструватись ' + error.status, 'ERROR');
    });
  }

}
