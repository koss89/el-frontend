import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import {ToastrModule} from 'ngx-toastr';


import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { RoutingModule } from './routing/routing.module';
import { HomeComponent } from './components/home/home.component';
import { ApiModule } from './api/api.module';
import { environment } from '../environments/environment';
import { AdminModule } from './admin/admin.module';
import { AuthorComponent } from './components/author/author.component';
import { UiComponentsModule } from './ui-components/ui-components.module';
import { BookListComponent } from './components/book-list/book-list.component';
import { BookComponent } from './components/book/book.component';
import { CommentComponent } from './components/comment/comment.component';
import { BookItemComponent } from './components/book-item/book-item.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthorComponent,
    BookListComponent,
    BookComponent,
    CommentComponent,
    BookItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AuthModule,
    RoutingModule,
    NoopAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
  }),
    ApiModule.forRoot({ apiUrl: environment.apiUrl }),
    AdminModule,
    UiComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
