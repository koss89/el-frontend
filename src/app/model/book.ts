import { Author } from './author';
import { Genre } from './genre';
import { Tag } from './tag';

export class Book {

    id: number;
    naz: string;
    dat: Date;
    description: string;
    izdat: string;
    img: string;
    file: string;
    rating: number;
    downloads: number;
    genreList: Array<Genre>;
    authorList: Array<Author>;
    tagList: Array<Tag>;

    constructor() {
        this.genreList = new Array();
        this.authorList = new Array();
        this.tagList = new Array();
    }
}

