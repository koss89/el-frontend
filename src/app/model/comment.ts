
export class Comment {

    id: number;
    idparent: Comment;
    dat: Date;
    message: string;
    subList: Array<Comment>;
    iduser: any;
    constructor() {
        this.subList = new Array();
    }
}

