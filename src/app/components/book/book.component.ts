import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from '../../api/book/book.service';
import { CommentService } from '../../api/comment/comment.service';
import { AuthService } from '../../auth/services/auth/auth.service';
import { Book } from '../../model/book';
import { Comment } from '../../model/comment';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @ViewChild('logoImage') image: ElementRef;

  book: Book = new Book();
  commentList: Array<Comment> = new Array();
  commentpage = 0;
  commenttotal = 0;

  rating: any;

  private sub: any;

  constructor(
    private _bookService: BookService,
    private _commentService: CommentService,
    private _authService: AuthService,
    private route: ActivatedRoute,
    private toast: ToastrService
  ) {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.loadBook(id);
        this.loadComments(id);
        this.loadRating(id);
      }

    });
  }

  ngOnInit() {
  }

  loadBook(id) {
    this._bookService.getById(id).subscribe(result => {
      this.book = result;
      this.loadImage();
    });
  }

  loadRating(id) {
    this._bookService.getRating(id).subscribe(result => {
      this.rating = result;
    });
  }

  loadComments(id) {
    this._commentService.getBookComments(this.commentpage, id).subscribe(result => {
      this.commentList = this.commentList.concat(result['content']);
      this.commenttotal = result['totalElements'];
    });
  }

  moreComments() {
    this.commentpage++;
    this.loadComments(this.book.id);
  }

  addRating(rate: number) {
    if (this.book.id) {
      this._bookService.setRating(this.book.id, rate).subscribe(() => {
        this.loadRating(this.book.id);
        this.toast.success('Успішно', 'Оцінка додана');
      });
    }
  }

  addComment(message: string) {
    const comment = new Comment();
    comment.message = message;
    this._commentService.save(comment, this.book.id).subscribe(() => {
      this.toast.success('Успішно', 'Коментар додано');
      this.commentList = new Array();
      this.loadComments(this.book.id);
    });
  }

  addSubComment(sub) {
    const comment = new Comment();
    comment.message = sub.message;
    this._commentService.saveSub(comment, this.book.id, sub.parent).subscribe(() => {
      this.toast.success('Успішно', 'Коментар додано');
      this.commentList = new Array();
      this.loadComments(this.book.id);
    });
  }

  loadImage() {
    if (this.book.img) {
      this._bookService.getFile(`/api/storage/files/${this.book.id}/${this.book.img}`).subscribe(response => {
        this.image.nativeElement.src = URL.createObjectURL(response);
      });
    }

  }

  download() {
    if (this.book.file) {
      this._bookService.getFile(`/api/storage/files/${this.book.id}/${this.book.file}`).subscribe(response => {
        const a = document.createElement('a'),
          fileURL = URL.createObjectURL(response);
        a.href = fileURL;
        a.download = this.book.file;
        window.document.body.appendChild(a);
        a.click();
        window.document.body.removeChild(a);
        URL.revokeObjectURL(fileURL);
      });
    }

  }

  editable(): Boolean {
    return this._authService.isAdmin();
  }

}
