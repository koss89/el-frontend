import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { BookService } from '../../api/book/book.service';
import { Book } from '../../model/book';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.css']
})
export class BookItemComponent implements OnInit {

  @Input()
  book: Book = new Book();

  @ViewChild('logoImage') image: ElementRef;

  constructor(
    private _bookService: BookService
  ) { }

  ngOnInit() {
    this.loadImage();
  }

  loadImage() {
    if (this.book.img) {
      this._bookService.getFile(`/api/storage/files/${this.book.id}/${this.book.img}`).subscribe(response => {
        this.image.nativeElement.src = URL.createObjectURL(response);
      });
    }

  }

}
