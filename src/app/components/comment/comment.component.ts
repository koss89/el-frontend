import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../model/comment';
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input()
  comment: Comment;

  subEditor = false;

  @Output()
  PublishComment = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  addSub() {
    this.subEditor = true;

  }

  addComment(message) {
    const sub = {
      parent : this.comment.id,
      message
    };
    this.PublishComment.emit(sub);

  }

  addSubComment(sub) {
    this.PublishComment.emit(sub);
  }

}
