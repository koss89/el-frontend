import { Component, OnInit } from '@angular/core';
import { BookService } from '../../api/book/book.service';
import { Book } from '../../model/book';
import { AuthorService } from '../../api/author/author.service';
import { GenreService } from '../../api/genre/genre.service';
import { TagService } from '../../api/tag/tag.service';
import { Author } from '../../model/author';
import { Genre } from '../../model/genre';
import { Tag } from '../../model/tag';
import { AuthService } from '../../auth/services/auth/auth.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  page = 0;
  pages: Array<number>;

  sort = {
    type: 'ASC',
    prop: 'dat'
  };

  filter = {
    naz: '',
    izdat: '',
    genreList: new Array<any>(),
    authorList: new Array<any>(),
    tagList: new Array<any>()
  };

  books: Array<Book>;
  genreList: Array<Genre>;
  authorList: Array<Author>;
  tagList: Array<Tag>;

  constructor(
    private _authService: AuthService,
    private _bookService: BookService,
    private _authorService: AuthorService,
    private _genreService: GenreService,
    private _tagService: TagService) { }

  ngOnInit() {
    this.loadAuthors();
    this.loadGenres();
    this.loadTags();
    this.getBooks();
  }

  setPage(i, event: any) {
    event.preventDefault();
    this.page = i;
    this.getBooks();
  }

  getBooks() {
    this._bookService.getBooks(this.page, this.sort).subscribe(result => {
      this.books = result['content'];
      this.pages = new Array(result['totalPages']);
    });
  }

  filterBook() {
    const filterbody = {
      naz: '',
      izdat: '',
      genreList: new Array<any>(),
      authorList: new Array<any>(),
      tagList: new Array<any>()
    };
    this.filter.authorList.forEach(element => {
      filterbody.authorList.push(element.id);
    });
    this.filter.genreList.forEach(element => {
      filterbody.genreList.push(element.id);
    });
    this.filter.tagList.forEach(element => {
      filterbody.tagList.push(element.id);
    });
    filterbody.naz = this.filter.naz;
    filterbody.izdat = this.filter.izdat;


    this._bookService.filter(this.page, this.sort, filterbody).subscribe(result => {
      this.books = result['content'];
      this.pages = new Array(result['totalPages']);
    });
  }

  loadAuthors() {
    this._authorService.getAll().subscribe(result => {
      this.authorList = result;
    });
  }

  loadGenres() {
    this._genreService.getAll().subscribe(result => {
      this.genreList = result;
    });
  }

  loadTags() {
    this._tagService.getAll().subscribe(result => {
      this.tagList = result;
    });
  }

  authorselection(selected) {
    this.filter.authorList = selected;
  }

  genreselection(selected) {
    this.filter.genreList = selected;
  }

  tagselection(selected) {
    this.filter.tagList = selected;
  }

  editable(): Boolean {
    return this._authService.isAdmin();
  }
}
