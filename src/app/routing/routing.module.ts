import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from '../auth/components/login/login.component';
import { RegisterComponent } from '../auth/components/register/register.component';
import { HomeComponent } from '../components/home/home.component';
import { BookComponent } from '../components/book/book.component';
import { BookEditorComponent } from '../admin/components/book-editor/book-editor.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'admin/book/:id', component: BookEditorComponent},
  { path: 'admin/book', component: BookEditorComponent},
  { path: 'book/:id', component: BookComponent},
  { path: '', component: HomeComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { useHash: true }),
  ],
  declarations: [],
  exports: [RouterModule]
})
export class RoutingModule { }
