import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MultiselectComponent } from './components/multiselect/multiselect.component';
import { CommentEditorComponent } from './components/comment-editor/comment-editor.component';
import { NgxEditorModule } from 'ngx-editor';
import { RatingComponent } from './components/rating/rating.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    NgxEditorModule
  ],
  declarations: [MultiselectComponent, CommentEditorComponent, RatingComponent],
  exports: [MultiselectComponent, CommentEditorComponent, RatingComponent]
})
export class UiComponentsModule { }
