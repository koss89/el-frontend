import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multiselect',
  templateUrl: './multiselect.component.html',
  styleUrls: ['./multiselect.component.css']
})
export class MultiselectComponent implements OnInit {

  @Input() list: Array<any>;

  @Input() editable = false;

  filteredList: Array<any> = new Array();

  @Input() selectedlist: Array<any> = new Array();

  @Input() labelfield: string;

  @Output()
  dataChange = new EventEmitter();

  query = '';

  constructor() { }

  ngOnInit() {
  }

  doSelect(item) {
    this.selectedlist.push(item);
    this.query = '';
    this.filteredList  = new Array();
    this.emmit();
  }

  remove(item) {
    this.selectedlist.splice(this.selectedlist.indexOf(item), 1);
    this.emmit();
  }

  emmit() {
    this.dataChange.emit(this.selectedlist);
  }

  filter() {
    this.filteredList = new Array();
    this.list.forEach(element => {
      if (element[this.labelfield].toUpperCase().includes(this.query.toUpperCase())) {
        this.filteredList.push(element);
      }
    });

  }

  getSelected() {
    return this.selectedlist;
  }

  close() {
    this.query = '';
    this.filteredList = new Array();
  }

  add() {
    const item = {};
    item[this.labelfield] = this.query;
    this.selectedlist.push(item);
    this.query = '';
    this.emmit();
  }

}
