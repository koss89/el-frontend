import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-comment-editor',
  templateUrl: './comment-editor.component.html',
  styleUrls: ['./comment-editor.component.css']
})
export class CommentEditorComponent implements OnInit {

  message = '';

  editorConfig = {
    'editable': true,
    'spellcheck': true,
    'height': 'auto',
    'minHeight': '350',
    'width': 'auto',
    'minWidth': '0',
    'translate': 'yes',
    'enableToolbar': true,
    'showToolbar': true,
    'placeholder': '...',
    'imageEndPoint': '',
    'toolbar': [
      ['bold', 'italic', 'underline', 'strikeThrough'],
      ['undo', 'redo'],
      ['horizontalLine', 'unorderedList'],
      ['link', 'image'],
      ['code']
    ]
  };

  @Output()
  PublishComment = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  publish() {
    this.PublishComment.emit(this.message);
    this.message = '';
  }
}
