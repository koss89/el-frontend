import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  @Input()
  rate = 0;

  @Input()
  max = 0;

  @Input()
  editable = false;

  @Output()
  clicked = new EventEmitter();

  rateArray: Array<number>;
  rateValArray: Array<number>;

  constructor() { }

  ngOnInit() {
    this.calculate();
  }

  calculate() {
    this.rateArray = new Array();
    for (let i = 1; i <= this.rate; i++) {
      this.rateArray.push(i + 1);
    }

    this.rateValArray = new Array();
    for (let i = this.max; i > this.rate; i--) {
      this.rateValArray.splice(0, 0, i);
    }
  }

  setRate(rate: number) {
    if (this.editable) {
      this.rate = rate;
      this.calculate();
      this.clicked.emit(rate);
    }
  }

}
