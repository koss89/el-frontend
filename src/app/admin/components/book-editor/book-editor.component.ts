import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BookService } from '../../../api/book/book.service';
import { AuthorService } from '../../../api/author/author.service';
import { GenreService } from '../../../api/genre/genre.service';
import { TagService } from '../../../api/tag/tag.service';
import { Author } from '../../../model/author';
import { Genre } from '../../../model/genre';
import { Tag } from '../../../model/tag';
import { Book } from '../../../model/book';

@Component({
  selector: 'app-book-editor',
  templateUrl: './book-editor.component.html',
  styleUrls: ['./book-editor.component.css']
})
export class BookEditorComponent implements OnInit, OnDestroy {

  book: Book = new Book();
  private sub: any;
  genreList: Array<Genre>;
  authorList: Array<Author>;
  tagList: Array<Tag>;

  constructor(
    private _bookService: BookService,
    private _authorService: AuthorService,
    private _genreService: GenreService,
    private _tagService: TagService,
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastrService
  ) { }

  ngOnInit() {

    this.loadAuthors();
    this.loadGenres();
    this.loadTags();

    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.loadBook(id);
      }

    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  loadBook(id) {
    this._bookService.getById(id).subscribe(result => {
      this.book = result;
    });
  }

  loadAuthors() {
    this._authorService.getAll().subscribe(result => {
      this.authorList = result;
    });
  }

  loadGenres() {
    this._genreService.getAll().subscribe(result => {
      this.genreList = result;
    });
  }

  loadTags() {
    this._tagService.getAll().subscribe(result => {
      this.tagList = result;
    });
  }

  authorselection(selected) {
    console.log('authorselection=' + JSON.stringify(selected));
    this.book.authorList = selected;
  }

  genreselection(selected) {
    this.book.genreList = selected;
  }

  tagselection(selected) {
    this.book.tagList = selected;
  }

  save() {
    this._bookService.save(this.book).subscribe(
      result => {
        this.toast.success('Успішно', 'Збережено!');
        const id = this.book.id;
        this.book = result;
        if (!id) {
          this.router.navigate([`/admin/book/${this.book.id}`]);
        }
      });
  }

  uploadImage(event): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const formData = new FormData();
      console.log('filename=' + file.name);
      formData.append('file', file, file.name);
      this._bookService.uploadfile(this.book.id, formData).subscribe(resp => {
        this.toast.success('Успішно', 'Збережено!');
      });
    }
  }

}
