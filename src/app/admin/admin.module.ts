import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BookEditorComponent } from './components/book-editor/book-editor.component';
import { UiComponentsModule } from '../ui-components/ui-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UiComponentsModule
  ],
  exports: [BookEditorComponent],
  declarations: [BookEditorComponent]
})
export class AdminModule { }
