
## Особенности запуска

API server Должен быть запущен по адрессу `http://localhost:8080/el`.  
Адресс можно изменить в  [Link](https://github.com/koss89/el-frontend/blob/f3cc3b8a5aa6a79d367791969f9148f3988f87f4/src/environments/environment.ts#L2) для dev режима и 
[Link](https://github.com/koss89/el-frontend/blob/f3cc3b8a5aa6a79d367791969f9148f3988f87f4/src/environments/environment.prod.ts#L2) для prod.

Собраный проект лежит в папке [dist](https://github.com/koss89/el-frontend/tree/master/dist)

# ElFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
